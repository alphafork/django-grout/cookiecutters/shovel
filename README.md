# Shovel - Django Project Template

[Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for
bootstrapping a Django project based on the
[django-grout](https://gitlab.com/alphafork/django-grout) meta framework.


## Quick Start

Install Cookiecutter if not already installed:

`pip install --user cookiecutter`

Generate Django project using the shovel cookiecutter:

`cookiecutter https://gitlab.com/alphafork/django-grout/cookiecutters/shovel`


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com/)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
